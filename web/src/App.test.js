import React from 'react';
import App from './App';
import {shallow} from 'enzyme';

describe('<App/>', () => {
    it('should render a Header', () => {
        const wrapper = shallow(<App/>);

        expect(wrapper.find('Header')).toExist();
    });

    it('should render HomePage', () => {
        const wrapper = shallow(<App/>);

        expect(wrapper.find('HomePage')).toExist();
    });
});
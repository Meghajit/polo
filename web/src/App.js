import React from 'react';
import HomePage from './Component/Login/HomePage';
import Header from './Component/StaticComponents/Header';

function App() {
    return (
        <div className="App">
            <Header/>
            <HomePage/>
        </div>
    );
}

export default App;
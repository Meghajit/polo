export const MAJORITY_STRATEGY = 'pointingStrategy/MAJORITY';
const getPointingStrategies = () => [MAJORITY_STRATEGY];

export const getStoryPoint = (pointingStrategy, pointingDetails) => {
    switch (pointingStrategy) {
        case MAJORITY_STRATEGY: {
            const pointMap = new Map();
            let maxKey = Number.NEGATIVE_INFINITY;
            let maxValue = 0;
            let consensusAchieved = false;

            pointingDetails.forEach((it) => {
                if (!pointMap.get(it.personPoint)) {
                    pointMap.set(it.personPoint, 1);
                } else {
                    const count = pointMap.get(it.personPoint);
                    pointMap.set(it.personPoint, count + 1);
                }
            });
            pointMap.forEach((value, key) => {
                if (value > maxValue) {
                    maxValue = value;
                    maxKey = key;
                    consensusAchieved = true;
                } else if (value === maxValue) {
                    consensusAchieved = false;
                }
            });
            return consensusAchieved ? maxKey : null;
        }
        default:
            return null;
    }
};

export default getPointingStrategies;
export const mockSucceedPromise = (result) => {
    return jest.fn(() => {
        return {
            then: (fun) => {
                fun(result);
                return {catch: () => {}};
            },
        };
    });
};

export const mockFailPromise = (result) => {
    return jest.fn(() => {
        return {
            then: () => {
                return {catch: (fun) => fun(result)};
            },
        };
    });
};

export const generateRandomString = () => Math.random().toString(36).substring(7);
export const generateRandomInteger = () => Math.floor(Math.random() * 100) + 1;
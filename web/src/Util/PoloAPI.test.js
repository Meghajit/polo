import PoloAPI from './PoloAPI';
import {generateRandomInteger, generateRandomString} from './testHelper';

describe('PoloApi', () => {
    describe('exposes method initializeStoryBoard', () => {
        const boardRoomId = generateRandomInteger();
        const io = require('socket.io')();

        beforeEach(() => {
            io.on('connect', function (socket) {
                socket.on('socketEvent/INITIALIZE_STORY_BOARD', (creatorName, callback) => {
                    callback(null, boardRoomId);
                });
            });
            io.listen(8080);
        });

        afterEach(() => {
            io.close();
        });

        it('should initialize story board with the creator name and return room id', (done) => {
            const poloAPI = new PoloAPI();

            const callback = (err, roomId) => {
                expect(err).toEqual(null);
                expect(roomId).toBe(boardRoomId);
                poloAPI.close();
                done();
            };

            poloAPI.initializeStoryBoard('Meghdoot', callback);
        });
    });

    describe('exposes method updateStoryBoardData', () => {
        const boardRoomId = generateRandomInteger();
        const creatorName = generateRandomString();
        const collaboratorName = generateRandomString();
        const collaboratorPoint = generateRandomInteger();
        const io = require('socket.io')();

        beforeEach(() => {
            io.on('connect', function (socket) {
                socket.on('socketEvent/INITIALIZE_STORY_BOARD', (creatorName, callback) => {
                    callback(null, boardRoomId);
                });
                socket.on(`socketEvent/UPDATE_STORY_BOARD_DATA/${boardRoomId}`, (updates, callback) => {
                    callback(null, {
                        [creatorName]: {point: 0},
                        [collaboratorName]: {point: collaboratorPoint},
                    })
                });
            });
            io.listen(8080);
        });

        afterEach(() => {
            io.close();
        });

        it('should update story board with pointing details', (done) => {
            const poloAPI = new PoloAPI();

            const callback = (err1, roomId) => {
                const updates = {
                    [collaboratorName]: {point: collaboratorPoint}
                };
                poloAPI.updateStoryBoardData(roomId, updates, (err2, boardData) => {
                    expect(err2).toBe(null);
                    expect(boardData).toEqual({
                        [creatorName]: {point: 0},
                        [collaboratorName]: {point: collaboratorPoint},
                    });
                    poloAPI.close();
                    done();
                });
            };

            poloAPI.initializeStoryBoard(creatorName, callback);
        });
    });

    describe('exposes method fetchStoryBoardData', () => {
        const creatorName = generateRandomString();
        const boardRoomId = generateRandomInteger();
        const io = require('socket.io')();

        beforeEach(() => {
            io.on('connect', function (socket) {
                socket.on('socketEvent/INITIALIZE_STORY_BOARD', (creatorName, callback) => {
                    callback(null, boardRoomId);
                });
                setTimeout(() => socket.emit(`socketEvent/FETCH_STORY_BOARD_DATA/${boardRoomId}`,
                    {
                        [creatorName]: {point: 0},
                    }), 1000);
            });
            io.listen(8080);
        });

        afterEach(() => {
            io.close();
        });

        it('should return the story board data with all pointing details', (done) => {
            const poloAPI = new PoloAPI();

            const callback = (err1, roomId) => {
                poloAPI.fetchStoryBoardData(roomId, (err2, boardData) => {
                    expect(err2).toBe(null);
                    expect(boardData).toEqual({
                        [creatorName]: {point: 0}
                    });
                    poloAPI.close();
                    done();
                });
            };

            poloAPI.initializeStoryBoard(creatorName, callback);
        });
    });
});
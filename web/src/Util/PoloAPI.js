import openSocket from 'socket.io-client';

class PoloAPI {
    constructor() {
        this.socket = openSocket('http://localhost:8080');
    }

    initializeStoryBoard = (creatorName, callback) => {
        this.socket.emit('socketEvent/INITIALIZE_STORY_BOARD', creatorName, (err, boardRoomId) => {
            callback(null, boardRoomId)
        });
    };

    fetchStoryBoardData = (boardRoomId, callback) => {
        this.socket.on(`socketEvent/FETCH_STORY_BOARD_DATA/${boardRoomId}`, (boardData) => {
            callback(null, boardData);
        });
    };

    updateStoryBoardData = (boardRoomId, updates, callback) => {
        this.socket.emit(`socketEvent/UPDATE_STORY_BOARD_DATA/${boardRoomId}`, updates, (err, boardData) => {
            callback(null, boardData)
        });
    };

    close() {
        this.socket.close();
    }
}

export default PoloAPI;


import {getStoryPoint, MAJORITY_STRATEGY} from './PointingStrategy';

describe('PointingStrategy', () => {
    describe('when strategy is MAJORITY', () => {
        it('returns the majority point when consensus is achieved', () => {
            const pointingDetails = [{
                personName: 'Tooki Tooki',
                personPoint: 4,
            },
                {
                    personName: 'Mooshi Tooki',
                    personPoint: 2,
                },
                {
                    personName: 'Pooki Pooki',
                    personPoint: 4,
                }];

            const storyPoint = getStoryPoint(MAJORITY_STRATEGY, pointingDetails);

            expect(storyPoint).toEqual(4)
        });

        it('returns null when consensus cannot be achieved', () => {
            const pointingDetails = [{
                personName: 'Tooki Tooki',
                personPoint: 4,
            },
                {
                    personName: 'Mooshi Tooki',
                    personPoint: 2,
                },
                {
                    personName: 'Tooki Mooshi',
                    personPoint: 2,
                },
                {
                    personName: 'Mooshi Mooshi',
                    personPoint: 4,
                }];

            const storyPoint = getStoryPoint(MAJORITY_STRATEGY, pointingDetails);

            expect(storyPoint).toEqual(null)
        });
    });

    it('returns null when no matching strategy', () => {
        const storyPoint = getStoryPoint('', {});

        expect(storyPoint).toBe(null);
    });
});
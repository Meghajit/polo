import React from 'react';
import PointStatus from './PointStatus';
import {shallow} from 'enzyme';

describe('<PointStatus/>', () => {
    describe('when props.votingStrategy is undefined', () => {
        it('should display that consensus has been achieved when a point achieves majority', () => {
            const pointingDetails = [{
                personName: 'Tooki Tooki',
                personPoint: 4,
            },
                {
                    personName: 'Mooshi Tooki',
                    personPoint: 2,
                },
                {
                    personName: 'Pooki Pooki',
                    personPoint: 4,
                }];
            const wrapper = shallow(<PointStatus pointingDetails={pointingDetails}/>);

            expect(wrapper.find('#consensus-message')).toHaveText('Consensus: ACHIEVED');
            expect(wrapper.find('#consensus-story-point')).toHaveText('Story Point: 4');
        });

        it('should display that consensus has not been achieved when no point achieves majority', () => {
            const pointingDetails = [{
                personName: 'Tooki Tooki',
                personPoint: 4,
            },
                {
                    personName: 'Mooshi Tooki',
                    personPoint: 2,
                },
                {
                    personName: 'Pooki Pooki',
                    personPoint: 8,
                }];
            const wrapper = shallow(<PointStatus pointingDetails={pointingDetails}/>);

            expect(wrapper.find('#consensus-message')).toHaveText('Consensus: NONE');
            expect(wrapper.find('#consensus-story-point')).toHaveText('Story Point: NA');
        });
    });
});
import React from 'react';
import PropTypes from 'prop-types';

const PointsTable = ({pointingDetails}) => {
    return (
        <table style={{
            width: '50%',
            paddingTop:'3%',
            marginLeft: 'auto',
            marginRight: 'auto'
        }}>
            <thead>
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Point
                </th>
            </tr>
            </thead>
            <tbody>
            {pointingDetails.map((pointingDetail, index) => {
                return (
                    <tr key={index}>
                        <td>
                            {pointingDetail.personName}
                        </td>
                        <td>
                            {pointingDetail.personPoint}
                        </td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
};

PointsTable.propTypes = {
    pointingDetails: PropTypes.arrayOf(
        PropTypes.exact({
            personName: PropTypes.string.isRequired,
            personPoint: PropTypes.number.isRequired,
        })).isRequired,
};

export default PointsTable;
import PropTypes from 'prop-types';
import React from 'react';
import getPointingStrategies, {getStoryPoint, MAJORITY_STRATEGY} from '../../Util/PointingStrategy';

const PointStatus = ({pointingDetails, pointingStrategy = MAJORITY_STRATEGY}) => {
    const storyPoint = getStoryPoint(pointingStrategy, pointingDetails);
    const consensusStatus = {
        message: storyPoint == null ? 'NONE' : 'ACHIEVED',
        point: storyPoint == null ? 'NA' : storyPoint
    };
    return (
        <div>
            <div id="consensus-message">
                Consensus: {consensusStatus.message}
            </div>
            <div id="consensus-story-point">
                Story Point: {consensusStatus.point}
            </div>
        </div>
    )
};

PointStatus.propTypes = {
    pointingDetails: PropTypes.arrayOf(
        PropTypes.exact({
            personName: PropTypes.string.isRequired,
            personPoint: PropTypes.number.isRequired,
        })).isRequired,
    pointingStrategy: PropTypes.oneOf(getPointingStrategies())
};

export default PointStatus;
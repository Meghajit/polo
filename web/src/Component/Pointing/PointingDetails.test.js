import PointingDetails from './PointingDetails';
import React from 'react';
import {shallow} from 'enzyme';

describe('<PointingDetails/>', () => {
    it('should render a PointStatus component', () => {
        const wrapper = shallow(<PointingDetails pointingDetails={[]}/>);

        expect(wrapper.find('PointStatus')).toHaveProp('pointingDetails', []);
    });

    it('should render a PointsTable component', () => {
        const pointingDetails = [
            {
                personName: 'Tooki Tooki',
                personPoint: 4,
            },
            {
                personName: 'Mooshi Tooki',
                personPoint: 2,
            },
            {
                personName: 'Pooki Pooki',
                personPoint: 8,
            }];
        const wrapper = shallow(<PointingDetails pointingDetails={pointingDetails}/>);

        expect(wrapper.find('PointsTable')).toHaveProp('pointingDetails', pointingDetails);
    });
});
import PointsTable from './PointsTable';
import React from 'react';
import {shallow} from 'enzyme';

describe('<PointsTable/>', () => {
    describe('should render a table', () => {
        it('should render two columns as Name and Point', () => {
            const wrapper = shallow(<PointsTable pointingDetails={[]}/>);

            expect(wrapper.find('table')).toExist();
            expect(wrapper.find('table thead tr th').at(0)).toHaveText('Name');
            expect(wrapper.find('table thead tr th').at(1)).toHaveText('Point');
        });

        it('should render rows from props.pointingDetails', () => {
            const pointingDetails = [{
                personName: 'Tooki Tooki',
                personPoint: 4,
            }];
            const wrapper = shallow(<PointsTable pointingDetails={pointingDetails}/>);


            const cellContainingName = wrapper.find('table tbody tr').find('td').first();
            const cellContainingPoint = wrapper.find('table tbody tr').find('td').last();
            expect(cellContainingName).toHaveText(pointingDetails[0].personName);
            expect(cellContainingPoint).toHaveText(pointingDetails[0].personPoint.toString());
        });
    });
});
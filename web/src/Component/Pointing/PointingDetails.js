import PropTypes from 'prop-types';
import PointStatus from './PointStatus';
import React from 'react';
import PointsTable from './PointsTable';

const PointingDetails = ({pointingDetails}) => {
    return (
        <div style={{textAlign: 'center'}}>
            <PointStatus pointingDetails={pointingDetails}/>
            <PointsTable pointingDetails={pointingDetails}/>
        </div>
    );
};

PointingDetails.propTypes = {
    pointingDetails: PropTypes.arrayOf(
        PropTypes.exact({
            personName: PropTypes.string.isRequired,
            personPoint: PropTypes.number.isRequired,
        })).isRequired,
};

export default PointingDetails;
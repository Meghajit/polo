import {shallow} from 'enzyme'
import React from 'react';
import Header from './Header';

describe('<Header/>', () => {
    it('should display the title Polo and a description', () => {
        const wrapper = shallow(<Header/>);

        expect(wrapper.find('#header')).toHaveText('Polo');
        expect(wrapper.find('#header-description')).toHaveText('A minimal story pointing tool for ' +
            'distributed teams, without the noise');
    });
});
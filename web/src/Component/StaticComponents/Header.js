import React from 'react';

const Header = () => {
    return (
        <div>
            <div id="header" style={{textAlign: 'center', fontSize: '3em'}}>Polo</div>
            <div id="header-description" style={{textAlign: 'center', fontSize: '1em'}}>
                A minimal story pointing tool for distributed teams, without the noise
            </div>
        </div>
    );
};

export default Header;
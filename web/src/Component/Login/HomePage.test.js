import {shallow} from 'enzyme';
import React from 'react';
import HomePage from './HomePage';

describe('<HomePage/>', () => {
    it('should render RoomActionButtonGroup with required props', () => {
        const wrapper = shallow(<HomePage/>);

        expect(wrapper.find('RoomActionButtonGroup')).toExist();
    });

    describe('when RoomActionButtonGroup invokes its prop onClickCreateBoardRoom', () => {
        it('should unmount RoomActionButtonGroup and mount UserDetailsForm', () => {
            const wrapper = shallow(<HomePage/>);

            wrapper.find('RoomActionButtonGroup').simulate('clickCreateBoardRoom');

            expect(wrapper.find('RoomActionButtonGroup')).not.toExist();
            expect(wrapper.find('UserDetailsForm')).toExist();

        });

        describe('when user submits User Details Form with his/her name', () => {
            it('should unmount UserDetailsForm and mount StoryBoard', () => {
                const wrapper = shallow(<HomePage/>);

                wrapper.find('RoomActionButtonGroup').simulate('clickCreateBoardRoom');
                wrapper.find('UserDetailsForm').simulate('submit', 'Zorro');

                expect(wrapper.find('UserDetailsForm')).not.toExist();
                expect(wrapper.find('StoryBoard')).toHaveProp({
                    boardRoomId: null,
                    personName: 'Zorro'
                });
            });
        });
    });

    describe('when RoomActionButtonGroup invokes its prop onClickJoinBoardRoom', () => {
        it('should unmount RoomActionButtonGroup and mount JoinRoomForm', () => {
            const wrapper = shallow(<HomePage/>);

            wrapper.find('RoomActionButtonGroup').simulate('clickJoinBoardRoom');

            expect(wrapper.find('RoomActionButtonGroup')).not.toExist();
            expect(wrapper.find('JoinRoomForm')).toExist();
        });

        describe('when user submits Join Room Form with his/her name and board room id', () => {
            it('should unmount JoinRoomForm and mount StoryBoard', () => {
                const wrapper = shallow(<HomePage/>);

                wrapper.find('RoomActionButtonGroup').simulate('clickJoinBoardRoom');
                wrapper.find('JoinRoomForm').simulate('submit', 'Zorro', 13);

                expect(wrapper.find('JoinRoomForm')).not.toExist();
                expect(wrapper.find('StoryBoard')).toHaveProp({
                    personName: 'Zorro',
                    boardRoomId: 13
                });
            });
        });
    });
});
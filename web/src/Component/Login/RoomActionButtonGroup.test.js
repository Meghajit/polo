import {shallow} from 'enzyme'
import React from 'react';
import RoomActionButtonGroup from './RoomActionButtonGroup';

const props = {
    onClickCreateBoardRoom: jest.fn(),
    onClickJoinBoardRoom: jest.fn(),
};

describe('<RoomActionButtonGroup/>', () => {
    afterEach(() => {
        props.onClickCreateBoardRoom.mockClear();
        props.onClickJoinBoardRoom.mockClear();
    });

    it('should display buttons for `Create Board Room and `Join Board Room`', () => {
        const wrapper = shallow(<RoomActionButtonGroup {...props}/>);

        expect(wrapper.find('button#create-board-room')).toHaveText('Create Board Room');
        expect(wrapper.find('button#join-board-room')).toHaveText('Join Board Room');
    });

    describe('on click of the `Create Board Room` button', () => {
        it('should call props.onClickCreateBoardRoom', () => {
            const wrapper = shallow(<RoomActionButtonGroup {...props}/>);

            wrapper.find('button#create-board-room').simulate('click');

            expect(props.onClickCreateBoardRoom).toHaveBeenCalled();
        });
    });

    describe('on click of the `Join Board Room` button', () => {
        it('should call props.onClickJoinBoardRoom', () => {
            const wrapper = shallow(<RoomActionButtonGroup {...props}/>);

            wrapper.find('button#join-board-room').simulate('click');

            expect(props.onClickJoinBoardRoom).toHaveBeenCalled();
        });
    });
});
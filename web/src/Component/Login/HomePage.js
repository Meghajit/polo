import React, {Component} from 'react';
import RoomActionButtonGroup from './RoomActionButtonGroup';
import UserDetailsForm from './UserDetailsForm';
import JoinRoomForm from './JoinRoomForm';
import StoryBoard from '../BoardRoom/StoryBoard';

class HomePage extends Component {
    #SHOW_CREATE_ROOM_FORM = 'homePage/showCreateRoomForm';
    #SHOW_JOIN_ROOM_FORM = 'homePage/showJoinRoomForm';
    #SHOW_STORY_BOARD = 'homePage/showStoryBoard';

    constructor(props) {
        super(props);
        this.state = {
            formView: null,
            personName: null,
            boardRoomId: null,
        }
    }

    setView = (formView) => {
        this.setState({formView});
    };

    handleSubmitUserDetailsForm = (creatorName) => {
        this.setState({
            personName: creatorName,
            formView: this.#SHOW_STORY_BOARD,
        });
    };

    handleSubmitJoinRoomForm = (joineeName, boardRoomId) => {
        this.setState({
            personName: joineeName,
            boardRoomId: boardRoomId,
            formView: this.#SHOW_STORY_BOARD,
        });
    };

    render() {
        const {formView, boardRoomId, personName} = this.state;
        switch (formView) {
            case this.#SHOW_CREATE_ROOM_FORM: {
                return <UserDetailsForm onSubmit={this.handleSubmitUserDetailsForm}/>
            }
            case this.#SHOW_JOIN_ROOM_FORM: {
                return <JoinRoomForm onSubmit={this.handleSubmitJoinRoomForm}/>
            }
            case this.#SHOW_STORY_BOARD: {
                return <StoryBoard boardRoomId={boardRoomId} personName={personName}/>
            }
            default: {
                return <RoomActionButtonGroup
                    onClickCreateBoardRoom={() => this.setView(this.#SHOW_CREATE_ROOM_FORM)}
                    onClickJoinBoardRoom={() => this.setView(this.#SHOW_JOIN_ROOM_FORM)}/>
            }
        }
    }
}

export default HomePage
import {shallow} from 'enzyme'
import React from 'react';
import UserDetailsForm from './UserDetailsForm';

const props = {
    onSubmit: jest.fn(),
};

describe('<UserDetailsForm/>', () => {
    it('renders a form with an input and submit button', () => {
        const wrapper = shallow(<UserDetailsForm {...props}/>);

        expect(wrapper.find('form')).toExist();
        expect(wrapper.find('input#user-name')).toExist();
        expect(wrapper.find('button')).toHaveText('Submit');
    });

    describe('clicking the submit button', () => {
        it('stops page from reloading and invokes props.onSubmit with the input value', () => {
            const wrapper = shallow(<UserDetailsForm {...props}/>);
            const event = {
                preventDefault: jest.fn(),
            };

            wrapper.find('input#user-name').simulate('change', {target: {value: 'Hello World'}});
            wrapper.find('button').simulate('click', event);

            expect(props.onSubmit).toHaveBeenCalledWith('Hello World');
            expect(event.preventDefault).toHaveBeenCalled();
        })
    });
});
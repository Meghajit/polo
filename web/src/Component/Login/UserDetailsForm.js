import React, {Component} from 'react';
import PropTypes from 'prop-types';

class UserDetailsForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: null,
        }
    }

    handleUserNameChange = (e) => {
        this.setState({userName: e.target.value});
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {onSubmit} = this.props;
        const {userName} = this.state;

        onSubmit(userName);
    };

    render() {
        return (
            <form style={{paddingTop: '3%'}}>
                <table cellSpacing="10" style={{
                    marginLeft: 'auto',
                    marginRight: 'auto',
                }}>
                    <tbody>
                    <tr>
                        <td>
                            Name
                        </td>
                        <td>
                            <input id="user-name" required onChange={this.handleUserNameChange}/>
                        </td>
                    </tr>
                    <tr style={{textAlign: 'center'}}>
                        <td colSpan="2">
                            <button style={{fontSize: '1em'}}
                                    type="submit"
                                    onClick={this.handleSubmit}>
                                Submit
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        )
    }
}

UserDetailsForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

export default UserDetailsForm;
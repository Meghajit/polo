import React from 'react';
import PropTypes from 'prop-types';

const RoomActionButtonGroup = ({onClickCreateBoardRoom, onClickJoinBoardRoom}) => {
    return (
        <table style={{
            marginLeft: 'auto',
            marginRight: 'auto',
            paddingTop: '3%'
        }}>
            <tbody>
            <tr>
                <td style={{paddingRight: '5%'}}>
                    <button style={{fontSize: '1em'}} id="create-board-room" onClick={onClickCreateBoardRoom}>
                        Create Board Room
                    </button>
                </td>
                <td style={{paddingLeft: '5%'}}>
                    <button style={{fontSize: '1em'}} id="join-board-room" onClick={onClickJoinBoardRoom}>
                        Join Board Room
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    );
};

RoomActionButtonGroup.propTypes = {
    onClickCreateBoardRoom: PropTypes.func.isRequired,
    onClickJoinBoardRoom: PropTypes.func.isRequired,
};

export default RoomActionButtonGroup;
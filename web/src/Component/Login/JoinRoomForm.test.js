import {shallow} from 'enzyme'
import React from 'react';
import JoinRoomForm from './JoinRoomForm';

const props = {
    onSubmit: jest.fn(),
};

describe('<JoinRoomForm/>', () => {
    it('should render a form with inputs for user name and board room id', () => {
        const wrapper = shallow(<JoinRoomForm {...props}/>);

        expect(wrapper.find('form')).toExist();
        expect(wrapper.find('input#user-name')).toExist();
        expect(wrapper.find('input#board-room-id')).toExist();
    });

    describe('clicking the submit button', () => {
        describe('when input values are valid', () => {
            it('stops page from reloading and invokes props.onSubmit with the input value', () => {
                const wrapper = shallow(<JoinRoomForm {...props}/>);
                const event = {
                    preventDefault: jest.fn(),
                };

                wrapper.find('input#user-name').simulate('change', {target: {value: 'Some user'}});
                wrapper.find('input#board-room-id').simulate('change', {target: {value: 13}});
                wrapper.find('button').simulate('click', event);

                expect(props.onSubmit).toHaveBeenCalledWith('Some user', 13);
                expect(event.preventDefault).toHaveBeenCalled();
            });
        });
    });
});
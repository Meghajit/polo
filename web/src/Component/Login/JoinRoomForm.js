import React, {Component} from 'react';
import PropTypes from 'prop-types';

class JoinRoomForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: null,
            boardRoomId: null,
        }
    }

    handleUserNameChange = (e) => {
        this.setState({userName: e.target.value});
    };

    handleBoardRoomIdChange = (e) => {
        this.setState({boardRoomId: e.target.value});
    };


    handleSubmit = (e) => {
        const {userName, boardRoomId} = this.state;
        e.preventDefault();
        const {onSubmit} = this.props;
        onSubmit(userName, boardRoomId);
    };

    render() {
        return (
            <form style={{paddingTop: '3%'}}>
                <table cellSpacing="10" style={{
                    marginLeft: 'auto',
                    marginRight: 'auto',
                }}>
                    <tbody>
                    <tr>
                        <td>
                            Name
                        </td>
                        <td>
                            <input id="user-name" required onChange={this.handleUserNameChange}/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Board Room ID
                        </td>
                        <td>
                            <input id="board-room-id" type="number" required onChange={this.handleBoardRoomIdChange}/>
                        </td>
                    </tr>
                    <tr style={{textAlign: 'center'}}>
                        <td colSpan="2">
                            <button style={{fontSize: '1em'}}
                                    type="submit"
                                    onClick={this.handleSubmit}>
                                Submit
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        );
    }
}

JoinRoomForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

export default JoinRoomForm;
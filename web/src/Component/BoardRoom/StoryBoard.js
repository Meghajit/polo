import React, {Component} from 'react';
import PointingDetails from '../Pointing/PointingDetails';
import PoloAPI from '../../Util/PoloAPI';
import PropTypes from 'prop-types';

class StoryBoard extends Component {
    constructor(props) {
        super(props);
        this.poloAPIHandler = new PoloAPI();
        this.state = {
            pointingDetails: [],
        }
    }

    fetchBoardData = (boardRoomId) => {
        const {personName} = this.props;
        const {updateStoryBoardData} = this.poloAPIHandler;
        const pointingDetails = [];

        updateStoryBoardData(boardRoomId, {[personName]: {point: 0}},
            (err, boardData) => {
                for (let key in boardData) {
                    if (boardData.hasOwnProperty(key)) {
                        pointingDetails.push({personName: key, personPoint: boardData[key].point})
                    }
                }
                this.setState({pointingDetails})
            }
        );
    };

    componentDidMount() {
        const {personName, boardRoomId} = this.props;
        const {initializeStoryBoard} = this.poloAPIHandler;
        if (boardRoomId == null) {
            initializeStoryBoard(personName, (err, roomId) => {
                this.fetchBoardData(roomId)
            });
        } else {
            this.fetchBoardData(boardRoomId)
        }
    }

    componentWillUnmount() {
        this.poloAPIHandler.close();
    }

    render() {
        const {pointingDetails} = this.state;
        return (
            <PointingDetails pointingDetails={pointingDetails}/>
        );
    }
}

StoryBoard.propTypes = {
    boardRoomId: PropTypes.number,
    personName: PropTypes.string.isRequired,
};

export default StoryBoard;
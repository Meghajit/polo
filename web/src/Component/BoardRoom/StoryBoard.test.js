jest.mock('../../Util/PoloAPI');

import PoloAPI from '../../Util/PoloAPI';
import StoryBoard from './StoryBoard';
import React from 'react';
import {shallow} from 'enzyme';

const props = {
    boardRoomId: null,
    personName: '',
};

describe('<StoryBoard/>', () => {
    describe('when gets mounted', () => {
        it('creates an instance of PoloAPI', () => {
            PoloAPI.mockImplementation(() => {
                return {
                    initializeStoryBoard: jest.fn(),
                }
            });

            shallow(<StoryBoard {...props}/>);

            expect(PoloAPI).toHaveBeenCalledTimes(1);
        });

        describe('props.boardRoomId is not null', () => {
            it('does not initialize a new board, updates board and renders PointingDetails with fetched data', () => {
                const newProps = {
                    ...props,
                    boardRoomId: 13,
                    personName: 'Wendy Carter',
                };
                const mockInitializeStoryBoard = jest.fn();
                const mockUpdateStoryBoardData = jest.fn().mockImplementation((roomId, updates, callback) => {
                    callback(null, {
                        'Creator Person': {point: 6},
                        ...updates,
                    });
                });
                PoloAPI.mockImplementation(() => {
                    return {
                        initializeStoryBoard: mockInitializeStoryBoard,
                        updateStoryBoardData: mockUpdateStoryBoardData,
                    }
                });

                const wrapper = shallow(<StoryBoard {...newProps} />);

                expect(mockInitializeStoryBoard).not.toHaveBeenCalled();
                expect(mockUpdateStoryBoardData).toHaveBeenCalledWith(newProps.boardRoomId,
                    {[newProps.personName]: {"point": 0}}, expect.any(Function));
                expect(wrapper.find('PointingDetails')).toHaveProp('pointingDetails', [
                    {
                        personName: 'Creator Person',
                        personPoint: 6,
                    },
                    {
                        personName: newProps.personName,
                        personPoint: 0,
                    }
                ]);
            });
        });

        describe('props.boardRoomId is null', () => {
            it('initializes a new board, updates board and renders PointingDetails with fetched data', () => {
                const newProps = {
                    ...props,
                    boardRoomId: null,
                    personName: 'Creator Person',
                };
                const mockInitializeStoryBoard = jest.fn().mockImplementation((creatorName, callback) => {
                    callback(null, 13);
                });
                const mockUpdateStoryBoardData = jest.fn().mockImplementation((roomId, updates, callback) => {
                    callback(null, {
                        [newProps.personName]: {point: 0},
                    });
                });
                PoloAPI.mockImplementation(() => {
                    return {
                        initializeStoryBoard: mockInitializeStoryBoard,
                        updateStoryBoardData: mockUpdateStoryBoardData,
                    }
                });

                const wrapper = shallow(<StoryBoard {...newProps} />);

                expect(mockInitializeStoryBoard).toHaveBeenCalledWith(newProps.personName, expect.any(Function));
                expect(mockUpdateStoryBoardData).toHaveBeenCalledWith(13,
                    {[newProps.personName]: {"point": 0}}, expect.any(Function));
                expect(wrapper.find('PointingDetails')).toHaveProp('pointingDetails', [
                    {
                        personName: 'Creator Person',
                        personPoint: 0,
                    },
                ]);
            });
        });
    });

    describe('when gets unmounted', () => {
        it('destroys the PoloAPI instance', () => {
            const mockClose = jest.fn();
            PoloAPI.mockImplementation(() => {
                return {
                    initializeStoryBoard: jest.fn(),
                    close: mockClose,
                }
            });

            const wrapper = shallow(<StoryBoard {...props}/>);
            wrapper.unmount();

            expect(mockClose).toHaveBeenCalledTimes(1);
        });
    });
});
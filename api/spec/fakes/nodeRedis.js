const redis = require('redis');

class FakeNodeRedis {
    constructor(fakeMethods) {
        this.mockNodeRedisClient = jasmine.createSpyObj('mockNodeRedisClient', ['on', 'get', 'set']);
        this.mockNodeRedisClient.on.and.callFake(fakeMethods.get('on'));
        this.mockNodeRedisClient.get.and.callFake(fakeMethods.get('get'));
        this.mockNodeRedisClient.set.and.callFake(fakeMethods.get('set'));
        spyOn(redis, 'createClient').and.returnValue(this.mockNodeRedisClient);
    }
}

module.exports = FakeNodeRedis;
const Jasmine = require('jasmine');
const jasmine = new Jasmine();
const reporters = require('jasmine-reporters');

jasmine.loadConfigFile('spec/support/jasmine.json');
jasmine.addReporter(new reporters.TerminalReporter({
    verbosity: 3,
    color: true,
    showStack: true
}));
jasmine.execute();
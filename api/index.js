const io = require('socket.io')();
const port = 8080;

io.on('connect', function (socket) {
    console.log('A user connected with id' + socket.id);
    setInterval(() => io.emit('socketEvent/GET_POINTING_DETAILS', [
        {
            personName: 'Chintamani',
            personPoint: Math.floor(Math.random() * Math.floor(5)),
        },
        {
            personName: 'Sukhmani',
            personPoint: Math.floor(Math.random() * Math.floor(5)),
        },
        {
            personName: 'Hasyamani',
            personPoint: Math.floor(Math.random() * Math.floor(5)),
        },
        {
            personName: 'Dukhmani',
            personPoint: Math.floor(Math.random() * Math.floor(5)),
        }
    ]), 1000)
});

io.listen(port);
console.log('Websocket listening on port ', port);
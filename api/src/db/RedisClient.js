const redis = require('redis');

class RedisClient {
    #client = null;

    constructor() {
        this.#client = redis.createClient();
        this.#client.on('connect', () => {
        });
    }

    setData = (key, payload, callback) => {
        this.#client.set(key, JSON.stringify(payload), (err) => {
            callback(err);
        });
    };

    getData = (key, callback) => {
        this.#client.get(key, (err, data) => {
            callback(null, JSON.parse(data));
        });
    }
}

exports.createClient = () => new RedisClient();
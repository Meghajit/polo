const BoardSession = require('./BoardSession');

describe('BoardSession', () => {
    it('should be able to create unique incremental board room ids', (done) => {
        const boardSession1 = new BoardSession();
        const boardSession2 = new BoardSession();

        boardSession1.createRoom('Holden Ford', (err1, roomId1) => {
            boardSession2.createRoom('Bill Tench', (err2, roomId2) => {

                expect(err1).toBeNull();
                expect(err2).toBeNull();
                expect(roomId2).toBe(roomId1 + 1);
                done();
            });
        });
    });

    it('should be able to retrieve the session data given a room id', (done) => {
        const boardSession = new BoardSession();

        boardSession.createRoom('Holden Ford', (err1, roomId) => {
            boardSession.getSessionData(roomId, (err2, data) => {
                expect(err2).toBeNull();
                expect(data).toEqual({
                    'Holden Ford': {
                        point: 0,
                    }
                });
                done();
            })
        });
    });

    it('should be able to update session data given a room id', (done) => {
        const boardSession = new BoardSession();

        boardSession.createRoom('Holden Ford', (err1, roomId) => {
            const sessionData = {
                'Bill Tench': {point: 4},
                'Holden Ford': {point: 6},
            };

            boardSession.setSessionData(roomId, sessionData, () => {
                boardSession.getSessionData(roomId, (err3, data) => {
                    expect(err3).toBeNull();
                    expect(data).toEqual({
                        'Holden Ford': {point: 6},
                        'Bill Tench': {point: 4},
                    });
                    done();
                });
            });
        });
    });
});
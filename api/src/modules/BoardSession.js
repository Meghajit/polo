const RedisClient = require('../db/RedisClient');
const BOARD_ROOM_ID_KEY = 'boardRoomId';

class BoardSession {
    #redisClient = null;
    createRoom = (creatorName, callback) => {
        this.#redisClient = RedisClient.createClient();
        this.#createBoardRoomId((roomId) => {
            const payload = {
                [creatorName]: {
                    point: 0,
                },
            };
            this.#redisClient.setData(roomId, payload, () => {
                callback(null, roomId);
            });
        });
    };

    getSessionData = (roomId, callback) => {
        this.#redisClient.getData(roomId, (err, data) => {
            callback(null, data);
        })
    };

    setSessionData = (roomId, payload, callback) => {
        this.getSessionData(roomId, (err, data) => {
            const updatedData = {
                ...data,
                ...payload,
            };
            this.#redisClient.setData(roomId, updatedData, () => {
                callback(null);
            });
        });
    };

    #createBoardRoomId = (roomIdCallback) => {
        const setBoardRoomId = (roomId, callback) => {
            const payload = {boardRoomId: roomId};
            this.#redisClient.setData(BOARD_ROOM_ID_KEY, payload, () => {
                callback(null);
            });
        };

        this.#redisClient.getData(BOARD_ROOM_ID_KEY, (err, payload) => {
            if (err) {
                setBoardRoomId(1, () => {
                    roomIdCallback(1)
                });
            } else {
                const nextBoardRoomId = payload.boardRoomId + 1;
                setBoardRoomId(nextBoardRoomId, () => {
                    roomIdCallback(nextBoardRoomId)
                });
            }
        });
    }
}

module.exports = BoardSession;